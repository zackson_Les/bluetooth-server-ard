#include <SoftwareSerial.h>

SoftwareSerial BTSerial(0, 1); // RX | TX

void setup()
{
  Serial.begin(115200);
  Serial.println("Enter AT commands:");
  BTSerial.begin(115200);  //gets changed to 115200 when the module is changed
}
void loop()
{
  if (BTSerial.available())
    Serial.write(BTSerial.read());
    
  if (Serial.available())
    BTSerial.write(Serial.read());
}
