#include <SoftwareSerial.h>
#include <Servo.h>
/*
  * bluetooth net with ard.
  * writen by Harel Lesnick.
 
                                                      * this is a simple text transfer on bluetooth sockets *
                                                                    * will inprove l8r. rofl. *
*/
Servo switch_servo;
char Incoming_value = 0;                //Variable for storing Incoming_value
static int pins[] = {8,9,10,11}; //rgb-led pins
int DEL = 1000;
String in_msg = "";
int leng_of_msg = 0;
String buff = "";
int c_spaces = 0;
SoftwareSerial blueTooth(1, 0);
void setup() 
{
  Serial.begin(9600);         //Sets the data rate in bits per second (baud) for serial data transmission
  blueTooth.begin(9600);
  switch_servo.attach(8);

  switch_servo.write(50);
  pinMode(1,OUTPUT);

  for(int i=0; i < 4; i++){
    pinMode(pins[i], OUTPUT);
    digitalWrite(pins[i], LOW);
  }
}

void loop()
{
  if(Serial.available() > 0)  
  {
    Incoming_value = Serial.read();      //Read the incoming data and store it into variable Incoming_value
    
    if(leng_of_msg > 0 && c_spaces >= 1){
      in_msg += Incoming_value;
    }
    else if(Incoming_value == ' ' && c_spaces < 1){
      leng_of_msg = buff.toInt();
      Serial.print(leng_of_msg);
      Serial.print("\n");
      buff = "";
      c_spaces += 1;
    }
    else{
      bool is_num = false;
      switch(Incoming_value){
          case '0':
            buff += Incoming_value;
          break;
          
          case '1':
            buff += Incoming_value;
          break;
          
          case '2':
            buff += Incoming_value;
          break;
          
          case '3':
            buff += Incoming_value;
          break;
          
          case '4':
            buff += Incoming_value;
          break;
          
          case '5':
            buff += Incoming_value;
          break;
          
          case '6':
            buff += Incoming_value;
          break;
          
          case '7':
            buff += Incoming_value;
          break;
          
          case '8':
            buff += Incoming_value;
          break;
          
          case '9':
            buff += Incoming_value;
          break;
          
          default:
            buff = "";
            leng_of_msg = 0;
            c_spaces = 0;
          break;
      }
      
      Serial.print(buff);
      Serial.print("\n");
    }
   // Serial.println(Incoming_value);        //Print Value of Incoming_value in Serial monitor
      
    if(in_msg.length() >= leng_of_msg-(c_spaces+1)){
      Serial.print(in_msg);
      Serial.print("\n");
      Serial.print(in_msg.length());
      leng_of_msg = 0;
      c_spaces = 0;
      
      if(in_msg.equals("LED-ON")){
          digitalWrite(pins[0], HIGH);
          digitalWrite(pins[1], HIGH);
          digitalWrite(pins[2], LOW);
          digitalWrite(pins[3], HIGH);
      }
      
      else if(in_msg.equals("LED-OFF")){      
          for(int i=0; i < 4; i++){
            digitalWrite(pins[i], LOW);
          }
      }

      else if(in_msg.equals("LED-RED")){
          digitalWrite(pins[0], LOW);
          digitalWrite(pins[1], LOW);
          digitalWrite(pins[2], LOW);
          digitalWrite(pins[3], HIGH);
      }

      else if(in_msg.equals("LED-BLUE")){
          digitalWrite(pins[0], LOW);
          digitalWrite(pins[1], HIGH);
          digitalWrite(pins[2], LOW);
          digitalWrite(pins[3], LOW);
      }

      else if(in_msg.equals("LED-GREEN")){
          digitalWrite(pins[0], HIGH);
          digitalWrite(pins[1], LOW);
          digitalWrite(pins[2], LOW);
          digitalWrite(pins[3], LOW);
      }

      else if(in_msg.equals("sw-on")){
          for (int pos = 0; pos <= 90; pos += 1) { 
            // in steps of 1 degree
            switch_servo.write(pos);              
            //delay(15);                       
          }
          delay(1000);
          switch_servo.write(80);
      }
      
      else if(in_msg.equals("sw-off")){
           for (int pos = 130; pos >= 0; pos -= 1) { 
            switch_servo.write(pos);              
            //delay(15);                       
          }
          delay(1000);
          switch_servo.write(50);
      }
      Serial.write(",ok");
      send_msg(",ok");
      in_msg = "";
    }
  } 
}
  void send_msg(String msg){
    for(int i=0; i < msg.length(); i+=1){
      blueTooth.write(msg[i]);
    }
  
} 
